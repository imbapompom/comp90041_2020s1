This repository contains the workshop slides of COMP90041 Programming and software development offered at the University of Melbourne in Semester 1, 2020.

The workshop slides are based on the predecessor's slides.

The slide will be posted here at the end of each week.

Please always refer to the official versions of the workshop solutions released on the Canvas if you are looking for an 'authoritative' answer for any particular question